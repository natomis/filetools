var fs  = require("fs"),
    crypto = require('crypto');

if(!process.argv[2]){
    console.log('Please specify an input file');
}

if(!process.argv[3]){
    console.log('Please specify an output file');
}


if (process.argv[2] && process.argv[3]){
    try{
        fs.unlinkSync(process.argv[3]);
    }
    catch(e){
        // yep we are gulping the error.
        console.log('Meh, the output file didn\'t exist');
    }


    fs.readFileSync(process.argv[2]).toString().split('\n').forEach(function (line) {
        var hash = crypto.createHash('md5').update(line).digest('hex') + '||';

        var newLine = hash.concat(line.toString());
        fs.appendFileSync(process.argv[3], newLine + "\n");
    });
};